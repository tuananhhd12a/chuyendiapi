﻿namespace APIBenXe.Model2
{
    public class ChuyenHuy
    {
        /// </summary>
        /// Mã chuyến đi
        /// </summary>
        public string MaChuyenDi { get; set; } = null!;
        /// <summary>
        /// Tên nhà xe
        /// </summary>
        public string TenNhaXe { get; set; } = null!;
        /// <summary>
        /// Số điện thoại nhà xe
        /// </summary>
        public string SoDienThoaiNhaXe { get; set; } = null!;
        /// <summary>
        /// Biển kiểm soát
        /// </summary>
        public string BienKiemSoat { get; set; } = null!;
        /// Ten Ben Di
        /// </summary>
        public string TenTinhDi { get; set; } = null!;
        /// <summary>
        /// Tên tỉnh đến
        /// </summary>
        public string TenTinhDen { get; set; } = null!;

        /// <summary>
        /// Thời gian khởi hành
        /// </summary>
        public DateTime? ThoiGianKhoiHanh { get; set; }
        /// <summary>
        /// Thời gian kết thúc
        /// </summary>
        public DateTime? ThoiGianKetThuc { get; set; }
        /// <summary>
        /// Lý do hủy chuyến đi
        /// </summary>
        public string? LyDoHuy { get; set; }
        /// <summary>
        /// Thời gian hủy
        /// </summary>
        public DateTime? ThoiGianHuy { get; set; }
    }
}
