﻿namespace APIBenXe.Model2
{
    public class ThemChuyen
    {
        public Guid IdTuyenDuong { get; set; }
        public string MaChuyenDi { get; set; } = null!;
        public List<string> DanhSachNhanVien { get; set; } = null!;
        public decimal? GiaVe { get; set; }
        public DateTime ThoiGianKhoiHanh { get; set; }
        public DateTime ThoiGianKetThuc { get; set; }
        public string TenNhaXe { get; set; } = null!;
        /// <summary>
        /// Số điện thoại nhà xe
        /// </summary>
        public string SoDienThoaiNhaXe { get; set; } = null!;
        /// <summary>
        /// Biển kiểm soát
        /// </summary>
        public string BienKiemSoat { get; set; } = null!;
        /// <summary>
        /// Số ghế
        /// </summary>
        public int SoGhe { get; set; }
        /// <summary>
        /// Số giường
        /// </summary>
        public int SoGiuong { get; set; }
    }
}
