﻿namespace APIBenXe.Model2
{
    public class ThemVe
    {
        public Guid? IdChuyenDi { get; set; }
        public string? HoTen { get; set; }
        public string? SoDienThoai { get; set; }
        public string? Email { get; set; }

    }
}
