﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class Huyen
    {
        public Huyen()
        {
            Benxes = new HashSet<Benxe>();
        }

        public Guid IdHuyen { get; set; }
        public string? TenHuyen { get; set; }

        public virtual ICollection<Benxe> Benxes { get; set; }
    }
}
