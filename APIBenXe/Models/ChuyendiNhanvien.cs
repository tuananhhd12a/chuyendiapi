﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class ChuyendiNhanvien
    {
        public Guid IdNhanVien { get; set; }
        public Guid? IdChuyenDi { get; set; }
        public string? HoTenNhanVien { get; set; }

        public virtual Chuyendi? IdChuyenDiNavigation { get; set; }
    }
}
