﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class Tinh
    {
        public Tinh()
        {
            Benxes = new HashSet<Benxe>();
        }

        public Guid IdTinh { get; set; }
        public string? TenTinh { get; set; }

        public virtual ICollection<Benxe> Benxes { get; set; }
    }
}
