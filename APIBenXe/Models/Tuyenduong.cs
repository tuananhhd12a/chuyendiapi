﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class Tuyenduong
    {
        public Tuyenduong()
        {
            Chuyendis = new HashSet<Chuyendi>();
        }

        public Guid IdTuyenDuong { get; set; }
        public Guid? IdBenXuatPhat { get; set; }
        public Guid? IdBenDen { get; set; }
        public string? MaTuyen { get; set; }
        public int? CuLy { get; set; }

        public virtual Benxe? IdBenDenNavigation { get; set; }
        public virtual Benxe? IdBenXuatPhatNavigation { get; set; }
        public virtual ICollection<Chuyendi> Chuyendis { get; set; }
    }
}
