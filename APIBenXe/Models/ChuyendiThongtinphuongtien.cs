﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class ChuyendiThongtinphuongtien
    {
        public Guid IdChuyenDi { get; set; }
        public string? TenNhaXe { get; set; }
        public string? SoDienThoaiNhaXe { get; set; }
        public string? BienKiemSoat { get; set; }
        public int? SoGhe { get; set; }
        public int? SoGiuong { get; set; }

        public virtual Chuyendi IdChuyenDiNavigation { get; set; } = null!;
    }
}
