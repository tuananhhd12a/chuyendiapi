﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class MuaVe
    {
        public MuaVe()
        {
            DoiSoats = new HashSet<DoiSoat>();
        }

        public Guid IdMuaVe { get; set; }
        public Guid? IdChuyenDi { get; set; }
        public Guid? IdHanhKhach { get; set; }
        public DateTime? ThoiGianMua { get; set; }

        public virtual Chuyendi? IdChuyenDiNavigation { get; set; }
        public virtual HanhKhach? IdHanhKhachNavigation { get; set; }
        public virtual ICollection<DoiSoat> DoiSoats { get; set; }
    }
}
