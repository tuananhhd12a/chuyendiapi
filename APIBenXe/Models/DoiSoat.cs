﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class DoiSoat
    {
        public Guid IdDoiSoat { get; set; }
        public Guid? IdMuaVe { get; set; }
        public DateTime? ThoiGianDoiSoat { get; set; }

        public virtual MuaVe? IdMuaVeNavigation { get; set; }
    }
}
