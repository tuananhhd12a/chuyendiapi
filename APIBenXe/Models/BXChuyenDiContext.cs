﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace APIBenXe.Models
{
    public partial class BXChuyenDiContext : DbContext
    {
        public BXChuyenDiContext()
        {
        }

        public BXChuyenDiContext(DbContextOptions<BXChuyenDiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Benxe> Benxes { get; set; } = null!;
        public virtual DbSet<Chuyendi> Chuyendis { get; set; } = null!;
        public virtual DbSet<ChuyendiNhanvien> ChuyendiNhanviens { get; set; } = null!;
        public virtual DbSet<ChuyendiThongtinphuongtien> ChuyendiThongtinphuongtiens { get; set; } = null!;
        public virtual DbSet<DoiSoat> DoiSoats { get; set; } = null!;
        public virtual DbSet<HanhKhach> HanhKhaches { get; set; } = null!;
        public virtual DbSet<Huyen> Huyens { get; set; } = null!;
        public virtual DbSet<MuaVe> MuaVes { get; set; } = null!;
        public virtual DbSet<Tinh> Tinhs { get; set; } = null!;
        public virtual DbSet<Trangthai> Trangthais { get; set; } = null!;
        public virtual DbSet<Tuyenduong> Tuyenduongs { get; set; } = null!;

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=10.10.0.200:54321;Database=BX.ChuyenDi;Username=dev;Password=123456");
            }
        }*/

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Benxe>(entity =>
            {
                entity.HasKey(e => e.IdBenXe)
                    .HasName("BENXE_pkey");

                entity.ToTable("BENXE");

                entity.Property(e => e.IdBenXe)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_BenXe");

                entity.Property(e => e.DiaChiCuThe).HasMaxLength(128);

                entity.Property(e => e.IdHuyen).HasColumnName("ID_Huyen");

                entity.Property(e => e.IdTinh).HasColumnName("ID_Tinh");

                entity.Property(e => e.TenBenXe).HasMaxLength(128);

                entity.HasOne(d => d.IdHuyenNavigation)
                    .WithMany(p => p.Benxes)
                    .HasForeignKey(d => d.IdHuyen)
                    .HasConstraintName("fk_BENXE_HUYEN_1");

                entity.HasOne(d => d.IdTinhNavigation)
                    .WithMany(p => p.Benxes)
                    .HasForeignKey(d => d.IdTinh)
                    .HasConstraintName("fk_BENXE_TINH_1");
            });

            modelBuilder.Entity<Chuyendi>(entity =>
            {
                entity.HasKey(e => e.IdChuyenDi)
                    .HasName("CHUYENDI_pkey");

                entity.ToTable("CHUYENDI");

                entity.Property(e => e.IdChuyenDi)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_ChuyenDi");

                entity.Property(e => e.GiaVe).HasPrecision(18);

                entity.Property(e => e.IdTrangThai).HasColumnName("ID_TrangThai");

                entity.Property(e => e.IdTuyenDuong).HasColumnName("ID_TuyenDuong");

                entity.Property(e => e.LyDoHuy).HasMaxLength(512);

                entity.Property(e => e.MaChuyenDi).HasMaxLength(50);

                entity.Property(e => e.ThoiGianHuy).HasColumnType("timestamp without time zone");

                entity.Property(e => e.ThoiGianKetThuc).HasColumnType("timestamp without time zone");

                entity.Property(e => e.ThoiGianKhoiHanh).HasColumnType("timestamp without time zone");

                entity.HasOne(d => d.IdTrangThaiNavigation)
                    .WithMany(p => p.Chuyendis)
                    .HasForeignKey(d => d.IdTrangThai)
                    .HasConstraintName("fk_CHUYENDI_TRANGTHAI_1");

                entity.HasOne(d => d.IdTuyenDuongNavigation)
                    .WithMany(p => p.Chuyendis)
                    .HasForeignKey(d => d.IdTuyenDuong)
                    .HasConstraintName("fk_CHUYENDI_TUYENDUONG_1");
            });

            modelBuilder.Entity<ChuyendiNhanvien>(entity =>
            {
                entity.HasKey(e => e.IdNhanVien)
                    .HasName("CHUYENDI_NHANVIEN_pkey");

                entity.ToTable("CHUYENDI_NHANVIEN");

                entity.Property(e => e.IdNhanVien)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_NhanVien");

                entity.Property(e => e.HoTenNhanVien).HasMaxLength(128);

                entity.Property(e => e.IdChuyenDi).HasColumnName("ID_ChuyenDi");

                entity.HasOne(d => d.IdChuyenDiNavigation)
                    .WithMany(p => p.ChuyendiNhanviens)
                    .HasForeignKey(d => d.IdChuyenDi)
                    .HasConstraintName("fk_CHUYENDI_NHANVIEN_CHUYENDI_1");
            });

            modelBuilder.Entity<ChuyendiThongtinphuongtien>(entity =>
            {
                entity.HasKey(e => e.IdChuyenDi)
                    .HasName("CHUYENDI_THONGTINPHUONGTIEN_pkey");

                entity.ToTable("CHUYENDI_THONGTINPHUONGTIEN");

                entity.Property(e => e.IdChuyenDi)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_ChuyenDi");

                entity.Property(e => e.BienKiemSoat).HasMaxLength(50);

                entity.Property(e => e.SoDienThoaiNhaXe).HasMaxLength(50);

                entity.Property(e => e.TenNhaXe).HasMaxLength(128);

                entity.HasOne(d => d.IdChuyenDiNavigation)
                    .WithOne(p => p.ChuyendiThongtinphuongtien)
                    .HasForeignKey<ChuyendiThongtinphuongtien>(d => d.IdChuyenDi)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_CHUYENDI_THONGTINPHUONGTIEN_CHUYENDI_1");
            });

            modelBuilder.Entity<DoiSoat>(entity =>
            {
                entity.HasKey(e => e.IdDoiSoat)
                    .HasName("DOI_SOAT_pkey");

                entity.ToTable("DOI_SOAT");

                entity.Property(e => e.IdDoiSoat)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_DoiSoat");

                entity.Property(e => e.IdMuaVe).HasColumnName("ID_MuaVe");

                entity.Property(e => e.ThoiGianDoiSoat).HasColumnType("timestamp without time zone");

                entity.HasOne(d => d.IdMuaVeNavigation)
                    .WithMany(p => p.DoiSoats)
                    .HasForeignKey(d => d.IdMuaVe)
                    .HasConstraintName("fk_DOI_SOAT_MUA_VE_1");
            });

            modelBuilder.Entity<HanhKhach>(entity =>
            {
                entity.HasKey(e => e.IdHanhKhach)
                    .HasName("HANH_KHACH_pkey");

                entity.ToTable("HANH_KHACH");

                entity.Property(e => e.IdHanhKhach)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_HanhKhach");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsFixedLength();

                entity.Property(e => e.HoTen)
                    .HasMaxLength(50)
                    .IsFixedLength();

                entity.Property(e => e.SoDienThoai)
                    .HasMaxLength(10)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Huyen>(entity =>
            {
                entity.HasKey(e => e.IdHuyen)
                    .HasName("HUYEN_pkey");

                entity.ToTable("HUYEN");

                entity.Property(e => e.IdHuyen)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_Huyen");

                entity.Property(e => e.TenHuyen).HasMaxLength(128);
            });

            modelBuilder.Entity<MuaVe>(entity =>
            {
                entity.HasKey(e => e.IdMuaVe)
                    .HasName("MUA_VE_pkey");

                entity.ToTable("MUA_VE");

                entity.Property(e => e.IdMuaVe)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_MuaVe");

                entity.Property(e => e.IdChuyenDi).HasColumnName("ID_ChuyenDi");

                entity.Property(e => e.IdHanhKhach).HasColumnName("ID_HanhKhach");

                entity.Property(e => e.ThoiGianMua).HasColumnType("timestamp without time zone");

                entity.HasOne(d => d.IdChuyenDiNavigation)
                    .WithMany(p => p.MuaVes)
                    .HasForeignKey(d => d.IdChuyenDi)
                    .HasConstraintName("fk_MUA_VE_CHUYENDI_1");

                entity.HasOne(d => d.IdHanhKhachNavigation)
                    .WithMany(p => p.MuaVes)
                    .HasForeignKey(d => d.IdHanhKhach)
                    .HasConstraintName("fk_MUA_VE_HANH_KHACH_1");
            });

            modelBuilder.Entity<Tinh>(entity =>
            {
                entity.HasKey(e => e.IdTinh)
                    .HasName("TINH_pkey");

                entity.ToTable("TINH");

                entity.Property(e => e.IdTinh)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_Tinh");

                entity.Property(e => e.TenTinh).HasMaxLength(128);
            });

            modelBuilder.Entity<Trangthai>(entity =>
            {
                entity.HasKey(e => e.IdTrangThai)
                    .HasName("TRANGTHAI_pkey");

                entity.ToTable("TRANGTHAI");

                entity.Property(e => e.IdTrangThai)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_TrangThai");

                entity.Property(e => e.TenTrangThai).HasMaxLength(128);
            });

            modelBuilder.Entity<Tuyenduong>(entity =>
            {
                entity.HasKey(e => e.IdTuyenDuong)
                    .HasName("TUYENDUONG_pkey");

                entity.ToTable("TUYENDUONG");

                entity.Property(e => e.IdTuyenDuong)
                    .ValueGeneratedNever()
                    .HasColumnName("ID_TuyenDuong");

                entity.Property(e => e.IdBenDen).HasColumnName("ID_BenDen");

                entity.Property(e => e.IdBenXuatPhat).HasColumnName("ID_BenXuatPhat");

                entity.Property(e => e.MaTuyen).HasMaxLength(50);

                entity.HasOne(d => d.IdBenDenNavigation)
                    .WithMany(p => p.TuyenduongIdBenDenNavigations)
                    .HasForeignKey(d => d.IdBenDen)
                    .HasConstraintName("fk_TUYENDUONG_BENXE_1");

                entity.HasOne(d => d.IdBenXuatPhatNavigation)
                    .WithMany(p => p.TuyenduongIdBenXuatPhatNavigations)
                    .HasForeignKey(d => d.IdBenXuatPhat)
                    .HasConstraintName("fk_TUYENDUONG_BENXE_2");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
