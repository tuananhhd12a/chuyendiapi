﻿using System;
using System.Collections.Generic;

namespace APIBenXe.Models
{
    public partial class Trangthai
    {
        public Trangthai()
        {
            Chuyendis = new HashSet<Chuyendi>();
        }

        public Guid IdTrangThai { get; set; }
        public string? TenTrangThai { get; set; }

        public virtual ICollection<Chuyendi> Chuyendis { get; set; }
    }
}
