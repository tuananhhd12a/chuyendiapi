

using APIBenXe.Models;
using APIBenXe.Services;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Serilog.Sinks.Graylog;

using System.Globalization;
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<BXChuyenDiContext>(
    o => o.UseNpgsql(builder.Configuration.GetConnectionString("BenXeConnection")));
builder.Services.AddScoped<QL_ChuyenDiServices>();
builder.Services.AddScoped<QL_VeChuyenDiServices>();

builder.Logging.ClearProviders();

var logger = new LoggerConfiguration()
            .ReadFrom.Configuration(builder.Configuration, sectionName: "Serilog")
            .WriteTo.Graylog("10.10.0.200", 33324, Serilog.Sinks.Graylog.Core.Transport.TransportType.Http)
            .WriteTo.Console()
            .CreateLogger();
builder.Logging.AddSerilog(logger);
builder.WebHost.UseSerilog(logger);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
