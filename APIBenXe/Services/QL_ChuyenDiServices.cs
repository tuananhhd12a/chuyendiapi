﻿
using APIBenXe.Model2;
using APIBenXe.Models;

namespace APIBenXe.Services
{
    public class QL_ChuyenDiServices
    {
        private readonly BXChuyenDiContext _context;
        private readonly ILogger<QL_ChuyenDiServices> _logger;

        public QL_ChuyenDiServices(BXChuyenDiContext context, ILogger<QL_ChuyenDiServices> logger)
        {
            _context = context;
            _logger = logger;

        }
        public Guid Ma_DangThuchien = new Guid("402418f5-687b-4635-9829-a5ccab33e91d");
        public Guid Ma_HoanThanh = new Guid("c8b27996-a001-4495-9320-4a6dbbc96d76");
        public Guid Ma_Huy = new Guid("6b616ec2-f68e-4d57-9aa9-16dca86f782c");
        public Guid IDBenThaiNguyen = new Guid("ee3de232-2fc4-469a-9110-6baf210f988e");
        //Cau_1
        public bool ThemChuyenDi(ThemChuyen input)
        {
            try
            {
                _logger.LogInformation("Thực Hiện Thêm Chuyến Đi");
                var chuyenDi = new Chuyendi
                {
                    IdChuyenDi = Guid.NewGuid(),
                    IdTrangThai = Ma_DangThuchien,
                    IdTuyenDuong = input.IdTuyenDuong,
                    ThoiGianKhoiHanh = input.ThoiGianKhoiHanh,
                    ThoiGianKetThuc = input.ThoiGianKetThuc,
                    LyDoHuy = "",
                    ThoiGianHuy = null,
                    MaChuyenDi = input.MaChuyenDi,
                    GiaVe = input.GiaVe,
                };
                var chuyenDi_PhuongTien = new ChuyendiThongtinphuongtien
                {
                    IdChuyenDiNavigation = chuyenDi,
                    IdChuyenDi = chuyenDi.IdChuyenDi,
                    BienKiemSoat = input.BienKiemSoat,
                    SoDienThoaiNhaXe = input.SoDienThoaiNhaXe,
                    SoGhe = input.SoGhe,
                    SoGiuong = input.SoGiuong,
                    TenNhaXe = input.TenNhaXe
                };
                foreach (var tennhanvien in input.DanhSachNhanVien)
                {
                    ChuyendiNhanvien nhanvien = new ChuyendiNhanvien()
                    {
                        IdChuyenDi = chuyenDi.IdChuyenDi,
                        HoTenNhanVien = tennhanvien,
                        IdNhanVien = Guid.NewGuid(),
                        IdChuyenDiNavigation = chuyenDi

                    };
                    _context.ChuyendiNhanviens.Add(nhanvien);
                }
                _context.Chuyendis.Add(chuyenDi);
                _context.ChuyendiThongtinphuongtiens.Add(chuyenDi_PhuongTien);
                _logger.LogInformation("Lưu Dữ liệu vào Database");
                _context.SaveChanges();
                _logger.LogInformation("Thêm Chuyến Đi Thành Công");
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return false;

            }

        }

        //Cau_2
        
        public bool ChuyenTrangThai(Guid id)
        {
            try
            {
                _logger.LogInformation("Thực Hiện ChuyenTrangThai");
                var chuyendi = _context.Chuyendis.FirstOrDefault(x => x.IdChuyenDi == id);
                if (chuyendi != null && chuyendi.IdTrangThai == Ma_DangThuchien)
                {
                    chuyendi.IdTrangThai = Ma_HoanThanh;
                    _context.SaveChanges();
                    _logger.LogInformation("ChuyenTrangThai Thành Công");
                    return true;

                }
                else
                {
                    _logger.LogInformation("ChuyenTrangThai Thất Bại");
                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }

        }
        //Cau_3
        
        public bool HuyChuyen(Guid id, HuyChuyenDi input)
        {
            try
            {
                _logger.LogInformation("Thực Hiện Hủy Chuyến Đi");
                var chuyendi = _context.Chuyendis.FirstOrDefault(x => x.IdChuyenDi == id);
                if (chuyendi != null && chuyendi.IdTrangThai == Ma_DangThuchien && chuyendi.LyDoHuy != null)
                {
                    chuyendi.IdTrangThai = Ma_Huy;
                    chuyendi.LyDoHuy = input.LyDoHuy;
                    chuyendi.ThoiGianHuy = DateTime.Now;
                    _context.SaveChanges();
                    _logger.LogInformation("Hủy Thành Công");
                    return true;

                }
                else
                {
                    _logger.LogInformation("Hủy Chuyến Thất Bại");
                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }

        }

        // Cau_4

        public List<ChuyenHoanThanh> DSChuyendiHoanThanh()
        {
            try
            {
                _logger.LogInformation("Thực Hiện DSChuyenDiHoanThanh");
                var DS_HoanThanh = from chuyendi in _context.Chuyendis
                                   join nhanvien in _context.ChuyendiNhanviens on chuyendi.IdChuyenDi equals nhanvien.IdChuyenDi
                                   join phuongtien in _context.ChuyendiThongtinphuongtiens on chuyendi.IdChuyenDi equals phuongtien.IdChuyenDi
                                   join tuyenduong in _context.Tuyenduongs on chuyendi.IdTuyenDuong equals tuyenduong.IdTuyenDuong
                                   join bendi in _context.Benxes on tuyenduong.IdBenXuatPhat equals bendi.IdBenXe
                                   join benden in _context.Benxes on tuyenduong.IdBenDen equals benden.IdBenXe
                                   where tuyenduong.IdBenXuatPhat == IDBenThaiNguyen && chuyendi.IdTrangThai == Ma_HoanThanh
                                   group new { chuyendi, nhanvien, phuongtien, tuyenduong, bendi, benden }
                                   by new
                                   {
                                       chuyendi.IdChuyenDi,
                                       chuyendi.MaChuyenDi,
                                       phuongtien.TenNhaXe,
                                       phuongtien.BienKiemSoat,
                                       phuongtien.SoDienThoaiNhaXe,
                                       bendi.TenBenXe,
                                       chuyendi.ThoiGianKhoiHanh,
                                       chuyendi.ThoiGianKetThuc
                                   } into groupChuyenDi
                                   select new ChuyenHoanThanh
                                   {
                                       MaChuyenDi = groupChuyenDi.Key.MaChuyenDi,
                                       TenNhaXe = groupChuyenDi.Key.TenNhaXe,
                                       BienKiemSoat = groupChuyenDi.Key.BienKiemSoat,
                                       SoDienThoaiNhaXe = groupChuyenDi.Key.SoDienThoaiNhaXe,
                                       Bendi = groupChuyenDi.Key.TenBenXe,
                                       Benden = groupChuyenDi.Select(k => k.benden.TenBenXe).FirstOrDefault(),
                                       HoTenNhanVien = String.Join(" - ", groupChuyenDi.Select(k => k.nhanvien.HoTenNhanVien)),
                                       ThoiGianKhoiHanh = groupChuyenDi.Key.ThoiGianKhoiHanh,
                                       ThoiGianKetThuc = groupChuyenDi.Key.ThoiGianKetThuc,
                                   };
                if (DS_HoanThanh == null)
                {
                    _logger.LogInformation("Không có chuyến đi nào");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return DS_HoanThanh.ToList();
            }
            catch (Exception e)
            {
                List<ChuyenHoanThanh> DS_Null = null;
                _logger.LogError(e, "Error");
                DS_Null = new List<ChuyenHoanThanh>();
                return DS_Null.ToList();
            }

        }
        //Cau_5
        public List<ChuyenHuy> DSChuyenHuy()
        {
            try
            {
                _logger.LogInformation("Thực Hiện DSChuyenHuy");
                var DS_Huy = from chuyenDi in _context.Chuyendis
                             join nhanVien in _context.ChuyendiNhanviens
                             on chuyenDi.IdChuyenDi equals nhanVien.IdChuyenDi
                             join phuongTien in _context.ChuyendiThongtinphuongtiens
                             on chuyenDi.IdChuyenDi equals phuongTien.IdChuyenDi
                             join trangThai in _context.Trangthais on chuyenDi.IdTrangThai equals trangThai.IdTrangThai
                             join tuyenDuong in _context.Tuyenduongs on chuyenDi.IdTuyenDuong equals tuyenDuong.IdTuyenDuong
                             join benDi in _context.Benxes on tuyenDuong.IdBenXuatPhat equals benDi.IdBenXe
                             join benDen in _context.Benxes on tuyenDuong.IdBenDen equals benDen.IdBenXe
                             join tinhDi in _context.Tinhs on benDi.IdTinh equals tinhDi.IdTinh
                             join tinhDen in _context.Tinhs on benDen.IdTinh equals tinhDen.IdTinh
                             where tuyenDuong.IdBenXuatPhat == IDBenThaiNguyen && trangThai.IdTrangThai == Ma_Huy
                             select new ChuyenHuy
                             {
                                 MaChuyenDi = chuyenDi.MaChuyenDi,
                                 TenNhaXe = phuongTien.TenNhaXe,
                                 BienKiemSoat = phuongTien.BienKiemSoat,
                                 SoDienThoaiNhaXe = phuongTien.SoDienThoaiNhaXe,
                                 TenTinhDen = tinhDen.TenTinh,
                                 TenTinhDi = tinhDi.TenTinh,
                                 ThoiGianKhoiHanh = chuyenDi.ThoiGianKhoiHanh,
                                 ThoiGianKetThuc = chuyenDi.ThoiGianKetThuc,
                                 ThoiGianHuy = chuyenDi.ThoiGianHuy,
                                 LyDoHuy = chuyenDi.LyDoHuy
                             };
                if (DS_Huy == null)
                {
                    _logger.LogInformation("Không có chuyến đi nào");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return DS_Huy.ToList();
            }
            catch (Exception e)
            {
                List<ChuyenHuy> DS_Null = null;
                _logger.LogError(e, "Error");
                DS_Null = new List<ChuyenHuy>();
                return DS_Null.ToList();
            }

        }

    }
}
