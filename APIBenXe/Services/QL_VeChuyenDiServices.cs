﻿

using APIBenXe.Model2;
using APIBenXe.Models;
using APIBenXe.Services.HeThong;
using System.Linq;

namespace APIBenXe.Services
{
    public class QL_VeChuyenDiServices
    {
        private readonly BXChuyenDiContext _context;
        private readonly ILogger<QL_VeChuyenDiServices> _logger;

        public QL_VeChuyenDiServices(BXChuyenDiContext context, ILogger<QL_VeChuyenDiServices> logger)
        {
            _context = context;
            _logger = logger;
        }

        public Guid Ma_DangThuchien = new Guid("402418f5-687b-4635-9829-a5ccab33e91d");
        //API Mua vé
        public bool MuaVeCD(ThemVe themVe)
        {
            try
            {
                _logger.LogInformation($"Thực hiện MuaVeCD - param={themVe.Object2Json()}");

                var hanhkhach = new HanhKhach {
                    IdHanhKhach = Guid.NewGuid(),
                    HoTen = themVe.HoTen,
                    SoDienThoai = themVe.SoDienThoai,
                    Email = themVe.Email
                };
                var muave = new MuaVe
                {
                    IdHanhKhachNavigation = hanhkhach,
                    IdMuaVe = Guid.NewGuid(),
                    IdChuyenDi = themVe.IdChuyenDi,
                    IdHanhKhach = hanhkhach.IdHanhKhach,
                    ThoiGianMua = DateTime.Now,
                };

                _logger.LogInformation("Kiểm tra");
                var sdtkt = _context.HanhKhaches.FirstOrDefault(k => k.SoDienThoai == themVe.SoDienThoai);
                var chuyendi = _context.Chuyendis.FirstOrDefault(x => x.IdChuyenDi == themVe.IdChuyenDi);
                if (chuyendi.IdTrangThai == Ma_DangThuchien && sdtkt == null) {
                    _context.MuaVes.Add(muave);
                    _context.HanhKhaches.Add(hanhkhach);
                    _logger.LogInformation("Lưu Dữ liệu vào Database");
                    _context.SaveChanges();
                    _logger.LogInformation("Kết thúc MuaVeCD");
                    return true;
                }
                else
                {
                    if (chuyendi.IdTrangThai == Ma_DangThuchien && sdtkt != null)
                    {

                        var updatehk = _context.HanhKhaches.FirstOrDefault(y => y.IdHanhKhach == sdtkt.IdHanhKhach);
                        updatehk.HoTen = themVe.HoTen;
                        updatehk.SoDienThoai = themVe.SoDienThoai;
                        updatehk.Email = themVe.Email;
                        var muaveupdate = new MuaVe
                        {
                            IdHanhKhachNavigation = updatehk,
                            IdMuaVe = Guid.NewGuid(),
                            IdChuyenDi = themVe.IdChuyenDi,
                            IdHanhKhach = sdtkt.IdHanhKhach,
                            ThoiGianMua = DateTime.Now,
                        };
                        _context.MuaVes.Add(muaveupdate);
                        _logger.LogInformation("Lưu Dữ liệu vào Database");
                        _context.SaveChanges();
                        _logger.LogInformation("Kết thúc MuaVeCD");
                        return true;
                    }
                    else
                    {
                        _logger.LogInformation("Trạng thái không hợp lệ");
                        return false;
                    }
                }

            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }
        }

        //API Đối Soát
        public bool DoiSoatCD(ThemDoiSoat themDoiSoat)
        {
            try
            {
                _logger.LogInformation($"Thực hiện DoiSoat - param={themDoiSoat.Object2Json()}");
                var ktidve = _context.MuaVes.FirstOrDefault(y => y.IdMuaVe == themDoiSoat.IdMuaVe && y.IdChuyenDi == themDoiSoat.IdChuyenDi);
                if (ktidve != null)
                {
                    _logger.LogInformation("Thành công");
                    var doisoatcd = new DoiSoat {
                        IdDoiSoat = Guid.NewGuid(),
                        IdMuaVe = themDoiSoat.IdMuaVe,
                        ThoiGianDoiSoat = DateTime.Now
                    };
                    _context.DoiSoats.Add(doisoatcd);
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    _logger.LogInformation("Vé không thuộc chuyến đi");
                    return false;
                }

            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
                return false;
            }

        }
        //API thống kê số lượng vé theo ChuyenDi
        public List<LKVeTheoCD> LietKeVeTheoCD(){
            try
            {
        
                _logger.LogInformation("Thực Hiện LietKeVeTheoCD");
                var lkslVeCD = from chuyendi in _context.Chuyendis
                                   join mve in _context.MuaVes on chuyendi.IdChuyenDi equals mve.IdChuyenDi
                                   join phuongtien in _context.ChuyendiThongtinphuongtiens on chuyendi.IdChuyenDi equals phuongtien.IdChuyenDi
                                   join tuyenduong in _context.Tuyenduongs on chuyendi.IdTuyenDuong equals tuyenduong.IdTuyenDuong
                                   join bendi in _context.Benxes on tuyenduong.IdBenXuatPhat equals bendi.IdBenXe
                                   join benden in _context.Benxes on tuyenduong.IdBenDen equals benden.IdBenXe
                                   group new { chuyendi, phuongtien, tuyenduong, bendi, benden ,mve}
                                   by new
                                   {
                                       chuyendi.GiaVe,
                                       chuyendi.MaChuyenDi,
                                       phuongtien.BienKiemSoat,
                                       bendi.TenBenXe,
                                       chuyendi.ThoiGianKhoiHanh,
                                       chuyendi.ThoiGianKetThuc,
                                       mve.IdChuyenDi

                                   } into groupChuyenDi
                                   select new LKVeTheoCD
                                   {
                                       ThongTinChuyenDi = groupChuyenDi.Key.MaChuyenDi + ", " +
                                       groupChuyenDi.Key.BienKiemSoat + ", " +
                                       groupChuyenDi.Key.TenBenXe + ", " +
                                       groupChuyenDi.Select(k => k.benden.TenBenXe).FirstOrDefault() + ", " +
                                       groupChuyenDi.Key.ThoiGianKhoiHanh + ", " +
                                       groupChuyenDi.Key.ThoiGianKetThuc,
                                       ThongTinVe = groupChuyenDi.Count(x => x.mve.IdChuyenDi == x.chuyendi.IdChuyenDi) + ", " +
                                       groupChuyenDi.Count(y => y.mve.IdChuyenDi == y.chuyendi.IdChuyenDi) * groupChuyenDi.Key.GiaVe,
                                   };
                if (lkslVeCD == null)
                {
                    _logger.LogInformation("Không có chuyến đi nào");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return lkslVeCD.ToList();
            }
            catch (Exception e)
            {
                List<LKVeTheoCD> DS_Null = null;
                _logger.LogError(e, "Error");
                DS_Null = new List<LKVeTheoCD>();
                return DS_Null.ToList();
            }
        }
        //API thống kê hành khách mua vé theo chuyen di
        public List<LKHanhKhachTheoCD> LietKeHanhKhachTheoCD()
        {
            try
            {

                _logger.LogInformation("Thực Hiện LietKeHanhKhachTheoCD");
                var lkHKCD = from chuyendi in _context.Chuyendis
                               join mve in _context.MuaVes on chuyendi.IdChuyenDi equals mve.IdChuyenDi
                               join phuongtien in _context.ChuyendiThongtinphuongtiens on chuyendi.IdChuyenDi equals phuongtien.IdChuyenDi
                               join tuyenduong in _context.Tuyenduongs on chuyendi.IdTuyenDuong equals tuyenduong.IdTuyenDuong
                               join bendi in _context.Benxes on tuyenduong.IdBenXuatPhat equals bendi.IdBenXe
                               join benden in _context.Benxes on tuyenduong.IdBenDen equals benden.IdBenXe
                               join hanhkhach in _context.HanhKhaches on mve.IdHanhKhach equals hanhkhach.IdHanhKhach
                               group new { chuyendi, phuongtien, tuyenduong, bendi, benden, mve , hanhkhach}
                               by new
                               {
                                   chuyendi.GiaVe,
                                   chuyendi.MaChuyenDi,
                                   phuongtien.BienKiemSoat,
                                   bendi.TenBenXe,
                                   chuyendi.ThoiGianKhoiHanh,
                                   chuyendi.ThoiGianKetThuc,
                                   mve.IdChuyenDi,
                                   hanhkhach.HoTen,
                                   hanhkhach.SoDienThoai,
                                   hanhkhach.Email

                               } into groupChuyenDi
                               select new LKHanhKhachTheoCD
                               {
                                   ThongTinChuyenDi = groupChuyenDi.Key.MaChuyenDi + ", " +
                                   groupChuyenDi.Key.BienKiemSoat + ", " +
                                   groupChuyenDi.Key.TenBenXe + ", " +
                                   groupChuyenDi.Select(k => k.benden.TenBenXe).FirstOrDefault() + ", " +
                                   groupChuyenDi.Key.ThoiGianKhoiHanh + ", " +
                                   groupChuyenDi.Key.ThoiGianKetThuc,
                                   ThongTinHanhKhach = String.Join(" /n ", groupChuyenDi.Key.HoTen +"|"+ groupChuyenDi.Key.SoDienThoai +"|"+ groupChuyenDi.Key.Email)
                                   
                               };
                if (lkHKCD == null)
                {
                    _logger.LogInformation("Không có chuyến đi nào");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return lkHKCD.ToList();
            }
            catch (Exception e)
            {
                List<LKHanhKhachTheoCD> DS_Null = null;
                _logger.LogError(e, "Error");
                DS_Null = new List<LKHanhKhachTheoCD>();
                return DS_Null.ToList();
            }
        }

        //API thống kê số lượng vé theo từng khách hàng 
        public List<LKVeTheoHK> LietKeVeTheoHK()
        {
            try
            {
                _logger.LogInformation("Thực Hiện DSVeKH");

                var ds = from hanhkhach in _context.HanhKhaches
                         join mve in _context.MuaVes on hanhkhach.IdHanhKhach equals mve.IdHanhKhach
                         join chuyendi in _context.Chuyendis on mve.IdChuyenDi equals chuyendi.IdChuyenDi
                         join phuongtien in _context.ChuyendiThongtinphuongtiens on chuyendi.IdChuyenDi equals phuongtien.IdChuyenDi
                         join tuyenduong in _context.Tuyenduongs on chuyendi.IdTuyenDuong equals tuyenduong.IdTuyenDuong
                         join bendi in _context.Benxes on tuyenduong.IdBenXuatPhat equals bendi.IdBenXe
                         join benden in _context.Benxes on tuyenduong.IdBenDen equals benden.IdBenXe
                         
                         group new { chuyendi, phuongtien, tuyenduong, bendi, benden, mve, hanhkhach }
                         by new
                         {
                             hanhkhach.HoTen,
                             hanhkhach.SoDienThoai,
                             hanhkhach.Email,
                             hanhkhach.IdHanhKhach,
                             chuyendi.MaChuyenDi,
                             phuongtien.BienKiemSoat,
                             bendi.TenBenXe,
  
                         } into groupCD
                         select new LKVeTheoHK
                         {
                             ThongTinHanhKhach = groupCD.Key.HoTen +" , "+
                             groupCD.Key.SoDienThoai +" , "+
                             groupCD.Key.Email,
                             DanhSachChuyenDiDaMuaVe = String.Join(" /n ", groupCD.Key.MaChuyenDi 
                             + " | " + groupCD.Key.BienKiemSoat 
                             + " | " + groupCD.Key.TenBenXe
                             + " | " + string.Join(",", groupCD.Select(x => x.benden.TenBenXe)))

                         };
                if (ds == null)
                {
                    _logger.LogInformation("Không có Vé");
                }
                else
                {
                    _logger.LogInformation("Liệt kê thành công");
                }
                return ds.ToList();
            }
            catch (Exception e)
            {
                List<LKVeTheoHK> DS_Null = null;
                _logger.LogError(e, "Error");
                DS_Null = new List<LKVeTheoHK>();
                return DS_Null.ToList();
            }
        }
    }
}
