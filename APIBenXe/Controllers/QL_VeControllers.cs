﻿using APIBenXe.Model2;
using APIBenXe.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIBenXe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QL_VeControllers : ControllerBase
    {
        private readonly QL_VeChuyenDiServices _QL_VeChuyenDi;
        public QL_VeControllers(QL_VeChuyenDiServices veChuyenDi)
        {
            _QL_VeChuyenDi = veChuyenDi;
        }
        [HttpPost("Mua_Ve")]
        public IActionResult MuaVeCD([FromBody]ThemVe themVe)
        {
            return Ok(_QL_VeChuyenDi.MuaVeCD(themVe));
        }
        [HttpPost("Doi_Soat")]
        public IActionResult DoiSoatCD([FromBody] ThemDoiSoat themDoiSoat)
        {
            return Ok(_QL_VeChuyenDi.DoiSoatCD(themDoiSoat));
        }
        [HttpGet("Liet_Ke_Ve_Theo_CD")]
        public IActionResult LietKeVeTheoCD()
        {
            return Ok(_QL_VeChuyenDi.LietKeVeTheoCD());
        }
        [HttpGet("Liet_Ke_Hanh_Khach_Theo_CD")]
        public IActionResult LietKeHanhKhachTheoCD()
        {
            return Ok(_QL_VeChuyenDi.LietKeHanhKhachTheoCD());
        }
        [HttpGet("Liet_Ke_Ve_Theo_Hanh_Khach")]
        public IActionResult LietKeVeTheoHK()
        {
            return Ok(_QL_VeChuyenDi.LietKeVeTheoHK());
        }
    }

}
